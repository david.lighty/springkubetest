package internal.ironhorse.library.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Exchange {
  private String name;
  private String url;
  private String type;
  private Locale locale;

  private void setStatus(String locale){
    this.locale = Locale.valueOf(locale.toUpperCase());
  }
}
