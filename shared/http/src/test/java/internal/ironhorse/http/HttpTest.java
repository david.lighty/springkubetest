package internal.ironhorse.http;

import internal.ironhorse.exceptions.RestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

class HttpTest {

    private Http http;
    private HttpClient mockHttpClient;

    @BeforeEach
    public void setup() {
        mockHttpClient = Mockito.mock(HttpClient.class);
    }

    @Test
    public void testGet() throws IOException, InterruptedException, RestException {
        HttpResponse getResponse = new HttpResponse() {
            @Override
            public int statusCode() {
                return 200;
            }

            @Override
            public HttpRequest request() {
                return null;
            }

            @Override
            public Optional<HttpResponse> previousResponse() {
                return Optional.empty();
            }

            @Override
            public HttpHeaders headers() {
                return null;
            }

            @Override
            public Object body() {
                return null;
            }

            @Override
            public Optional<SSLSession> sslSession() {
                return Optional.empty();
            }

            @Override
            public URI uri() {
                return null;
            }

            @Override
            public HttpClient.Version version() {
                return null;
            }
        };
        when(mockHttpClient.send(any(), any()))
                .thenReturn(getResponse);

        try (MockedStatic mocked = mockStatic(HttpClient.class)) {
            mocked.when(HttpClient::newHttpClient).thenReturn(mockHttpClient);
            http = new Http();
            http.setBaseURL("http://localhost");
            HttpResponse response = http.GET("/test");
            Assertions.assertTrue(response.statusCode() == 200);
        }
    }

    @Test
    public void testMissingEndpointRequirements(){
        try (MockedStatic mocked = mockStatic(HttpClient.class)) {
            mocked.when(HttpClient::newHttpClient).thenReturn(mockHttpClient);
            http = new Http();
            http.setBaseURL("http://localhost");
            Assertions.assertThrows(RestException.class, () -> http.GET("test"));
        }
    }
    
}