package internal.ironhorse.exceptions;

public class RestException extends Exception{
    public RestException(String error_with_rest_client, Exception e) {
        super(error_with_rest_client, e);
    }

    public RestException(String error_with_rest_client) {
        super(error_with_rest_client);
    }

    public RestException(Throwable t) {
        super(t.getMessage());
        this.setStackTrace(t.getStackTrace());
    }
}
