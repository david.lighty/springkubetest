package internal.ironhorse.topics;

import org.apache.kafka.clients.admin.NewTopic;


public enum Topic {
  STOCK_TICKER("stock-ticker", 3, 1),
  STOCK_SEARCH("stock-search", 3, 1);

  String name;
  int numPartitions = 3;
  int replicationFactor = 1;

  Topic(String name, int numPartitions, int replicationFactor) {
    this.name = name;
    this.numPartitions = numPartitions;
    this.replicationFactor = replicationFactor;
  }

  public String getTopicName(){
    return this.name;
  }

  public NewTopic toNewTopic() {
    return DefaultTopicFactory.getBasicTopic(this.name, this.numPartitions, this.replicationFactor);
  }
}
