package internal.ironhorse.topics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

public class TopicProducerFactory {

    ProducerFactory producerFactory;

    @Autowired
    TopicProducerFactory(ProducerFactory producerFactory){
        this.producerFactory = producerFactory;
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory);
    }
}
