package internal.ironhorse.topics;

import java.util.Arrays;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.kafka.clients.admin.AdminClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

@Configuration
public class TopicAdministrator {
  public final Logger log = Logger.getLogger(TopicAdministrator.class.getSimpleName());
  private final AdminClient adminClient;

  public TopicAdministrator(KafkaAdmin kafkaAdmin) {
    log.info("Init Topics");
     this.adminClient = AdminClient.create(kafkaAdmin.getConfigurationProperties());
      initTopics();
  }

  public void initTopics(){
    adminClient.createTopics(Arrays.stream(Topic.values())
        .map(Topic::toNewTopic)
        .collect(Collectors.toList())
    ).all();
  }

}
