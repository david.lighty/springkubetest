package internal.ironhorse.config;

import org.springframework.context.annotation.Configuration;

@Configuration(value="ironhorse.kafka")
public class KafkaConfiguration {
    private String bootstrapServers;
    private String groupId;
}