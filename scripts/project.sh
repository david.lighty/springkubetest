#!/bin/bash

#Local Run/Build script

AR=">>"

fnJDK() {
  jabba use amazon-corretto@1.11.0-11.9.1
}

fnVERSION() {
  MVN_VERSION=$(mvn \
    help:evaluate \
    -Dexpression=project.version \
    -q -DforceStdout)
}

fnPROJECT() {
  MVN_PROJECT=$(mvn \
    help:evaluate \
    -Dexpression=project.artifactId \
    -q -DforceStdout)
}

fnClean() {
  echo "$AR :CLEAN"
  mvn clean -DskipTests
}

fnBuild() {
  echo "$AR $1:BUILD"
  OPTS="-pl $1"
  if [ "$MODULE" == "all" ]; then
    OPTS=""
  fi
  mvn -T 4C install compile $OPTS -DskipTests --am
}

fnTest() {
  echo "$AR $1:TEST"
  OPTS="-pl $1"
  if [ "$MODULE" == "all" ]; then
    OPTS=""
  fi
  mvn test verify $OPTS
}

fnBuildImage() {
  echo "$AR BUILD IMAGE(S)"
  OPTS=("$1")
  if [ "$MODULE" == "all" ]; then
    OPTS=("infrastructure/gateway-service"
      "infrastructure/service-discovery"
      "services/stock-info-service"
    )
  fi
  for M in "${OPTS[@]}"; do
    echo "$AR $M:BUILD-IMAGE"
    mvn spring-boot:build-image -pl $M -DskipTests
  done
}

fnINFRA() {
  docker-compose -f infrastructure-compose.yml $1 $2
}

fnSERVICES() {
  docker-compose -f services-compose.yml $1 $2
}

fnUP() {
  echo "$AR UP:INFRASTRUCTURE"
  fnINFRA up -d
  fnSERVICES up -d
  docker ps
}

fnDOWN() {
  echo "$AR DOWN:INFRASTRUCTURE"
  fnINFRA down
  fnSERVICES down
}

# MAIN ENTRY
fnVERSION
fnPROJECT
clear
echo "$MVN_PROJECT:$MVN_VERSION BUILD/RUN Helper"
while getopts "udb:t:i:j" opt; do
  case ${opt} in
  b)
    MODULE=${OPTARG}
    echo "BUILD $MODULE"
    fnBuild $MODULE
    ;;
  j)
    fnJDK
    ;;
  t)
    MODULE=${OPTARG}
    echo "TEST $MODULE"
    fnTest $MODULE
    ;;
  i)
    MODULE=${OPTARG}
    case "$MODULE" in
    *shared*)
      echo "$MODULE Not supported for imaging."
      exit 0
      ;;
    esac
    echo "IMAGE $MODULE"
    fnBuildImage $MODULE
    ;;
  u)
    fnUP
    ;;
  d)
    fnDOWN
    ;;
  \?)
    echo "-b <module> -t <module> -i <module> -u"
    ;;
  esac
done
