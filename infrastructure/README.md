# Infrastructure

Module to hold basic infrastructure
modules for running our apps.  Mostly local 
service discovery/load balancer/etc type modules.

There should be zero logic/API type modules here.

References
----------
* Services - All microservices
* Shared - All sharable items