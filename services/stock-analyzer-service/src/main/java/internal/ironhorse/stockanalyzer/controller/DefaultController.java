package internal.ironhorse.stockanalyzer.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    @GetMapping(path = "/info")
    public ResponseEntity<?> getInfo() {
        return ResponseEntity.ok("info");
    }
}
