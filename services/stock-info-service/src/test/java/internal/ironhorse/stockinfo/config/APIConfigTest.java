package internal.ironhorse.stockinfo.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;


@SpringBootTest
@TestPropertySource(
        properties = {
                "api.key=test"
        }
)
class APIConfigTest {

    @Autowired
    APIConfig config;

    @Test
    public void hasConfig() {
        String expectedKey = "test";
        String acutalKey = config.getKey();
        Assertions.assertEquals(expectedKey, acutalKey, "Missing expected key");
    }
}