package internal.ironhorse.stockinfo.polygon.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EndpointsTest {
    @Test
    public void getTicker() {
        String expected = "v3/reference/tickers";
        String actual = Endpoints.TICKERS.getEndpoint();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getExchanges() {
        String expected = "v3/reference/exchanges";
        String actual = Endpoints.EXCHANGE.getEndpoint();
        Assertions.assertEquals(expected, actual);
    }
}