package internal.ironhorse.stockinfo.polygon.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EndpointBuilderTest {

    @Test
    public void testConstructsQueryString() throws Exception {
        String expected = "/test?q=1&apiKey=testKey";
        EndpointBuilder eb = EndpointBuilder.newBuilder();
        eb.addSection("test")
                .addParam("q", "1")
                .addKey("testKey");
        Assertions.assertEquals(expected, eb.build());
    }

    @Test
    public void testEmptyParamsAllowed() throws Exception {
        String expected = "/test?&apiKey=testKey";
        EndpointBuilder eb = EndpointBuilder.newBuilder()
                .addKey("testKey")
                .addSection("test");
        Assertions.assertEquals(expected, eb.build());
    }

    @Test
    public void testEmptySectionsAllowed() throws Exception {
        String expected = "?q=1&apiKey=testKey";
        EndpointBuilder eb = EndpointBuilder.newBuilder()
                .addKey("testKey")
                .addParam("q", "1");
        Assertions.assertEquals(expected, eb.build());
    }


    @Test
    public void testEmptyEndpointNotAllowed() throws Exception {
        String notExpected = "?apiKey=testKey";
        EndpointBuilder eb = EndpointBuilder.newBuilder()
                .addKey("testKey");
        Assertions.assertThrows(Exception.class, () -> {
            eb.build();
        });
    }

    @Test
    public void assertThrowsMissingKey() {
        Assertions.assertThrows(Exception.class, () -> {
            EndpointBuilder.newBuilder().build();
        });
    }
}