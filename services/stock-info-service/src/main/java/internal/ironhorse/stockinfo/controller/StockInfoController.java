package internal.ironhorse.stockinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.library.api.Stock;
import internal.ironhorse.stockinfo.service.APIClientService;

/**
 * Entry point for all APIClientLayer calls.
 */
@RestController
@CrossOrigin
public class StockInfoController {
    private final APIClientService clientLayer;

    @Autowired
    public StockInfoController(APIClientService clientLayer) {
        this.clientLayer = clientLayer;
    }

    @GetMapping(path = "/info/{symbol}")
    public ResponseEntity<?> getCompany(
            @PathVariable String symbol) throws RestException {
        Stock stock = this.clientLayer.getStockInfo(symbol);
        return ResponseEntity.ok(stock);
    }

    @GetMapping(path = "/info/{symbol}/news")
    public ResponseEntity<?> getNews(
            @PathVariable String symbol) throws RestException {
        Stock stock = this.clientLayer.getStockInfo(symbol);
        return ResponseEntity.ok(stock);
    }

}
