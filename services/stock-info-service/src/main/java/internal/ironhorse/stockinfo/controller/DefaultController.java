package internal.ironhorse.stockinfo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    @GetMapping(path = "/test")
    public ResponseEntity<?> getInfo() {
        return ResponseEntity.ok("test");
    }

}
