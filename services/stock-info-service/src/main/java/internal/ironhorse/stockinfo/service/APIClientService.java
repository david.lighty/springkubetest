package internal.ironhorse.stockinfo.service;

import java.util.List;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.library.api.Exchange;
import internal.ironhorse.library.api.Stock;

public interface APIClientService {
    List<Stock> searchStockInfo(String testTickerOrName) throws Exception;
    Stock getStockInfo(String stockTicker) throws RestException;
    void getStockNews(String stockTicker);
    Exchange getExchange(String name);
    List<Exchange> getExchanges() throws RestException;
}
