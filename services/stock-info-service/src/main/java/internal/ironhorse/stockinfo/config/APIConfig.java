package internal.ironhorse.stockinfo.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "api")
public class APIConfig {
    private final static String HOSTNAME_PORT_FORMAT = "https://%s:%s";
    private final static String HOSTNAME_FORMAT = "https://%s";
    private String hostname;
    private String port;
    private String version;
    private String key;


    public String constructAPIEndpoint(){
        return StringUtils.isBlank(port) ?
                String.format(HOSTNAME_FORMAT, hostname) :
                String.format(HOSTNAME_PORT_FORMAT, hostname, port);
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
