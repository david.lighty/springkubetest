package internal.ironhorse.stockinfo;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ConfigurationPropertiesScan
@ComponentScan(
    basePackages = {
        "internal.ironhorse.http",
        "internal.ironhorse.stockinfo",
        "internal.ironhorse.topics",
        "internal.ironhorse.util"
    }
)
public class StockInfoService {

  private static final Logger log = LoggerFactory.getLogger(StockInfoService.class);
  public static String SERVICE_NAME = "Stock Info Service";

  public static void main(String[] args) {
    ApplicationContext cxt = SpringApplication.run(StockInfoService.class, args);
  }

  @PostConstruct
  public void startup() {
    log.info(SERVICE_NAME + " STARTED");
  }
}
