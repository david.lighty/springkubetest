package internal.ironhorse.stockinfo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import internal.ironhorse.stockinfo.domain.EventMessage;
import internal.ironhorse.topics.Topic;
import internal.ironhorse.topics.TopicAdministrator;
import internal.ironhorse.util.GsonUtil;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class EventHandlerService {

  private final TopicAdministrator topicAdministrator;
  private KafkaTemplate<String, String> kafkaTemplate;
  private GsonUtil gsonUtil;

  @Autowired
  public EventHandlerService(KafkaTemplate<String, String> kafkaTemplate, GsonUtil gsonUtil,
      TopicAdministrator topicAdministrator) {
    this.kafkaTemplate = kafkaTemplate;
    this.gsonUtil = gsonUtil;
    this.topicAdministrator = topicAdministrator;
  }

  @Async("threadPoolTaskExecutor")
  public <K, V extends EventMessage> void sendMessage(Topic topic, K key, V content) {
    String keyJson = gsonUtil.toJson(key);
    String contentJson = gsonUtil.toJson(content);
    log.info("Send message: " + topic.getTopicName());
    kafkaTemplate.send(topic.getTopicName(), keyJson, contentJson);
  }
}
