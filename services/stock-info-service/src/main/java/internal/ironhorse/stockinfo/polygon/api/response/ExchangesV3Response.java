package internal.ironhorse.stockinfo.polygon.api.response;

import java.util.List;

import internal.ironhorse.stockinfo.polygon.api.Exchange;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class ExchangesV3Response extends APIV1PageResponse {
    private List<Exchange> results;
}
