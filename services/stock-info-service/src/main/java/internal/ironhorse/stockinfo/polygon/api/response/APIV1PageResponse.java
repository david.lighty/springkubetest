package internal.ironhorse.stockinfo.polygon.api.response;

public abstract class APIV1PageResponse extends APIV1Response{
    private Integer count;
    private String nextUrl;

    @Override
    public String toString() {
        return "APIV1PageResponse{" +
            "count=" + count +
            ", nextUrl='" + nextUrl + '\'' +
            '}';
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
