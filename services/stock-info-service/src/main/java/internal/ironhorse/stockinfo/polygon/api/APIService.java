package internal.ironhorse.stockinfo.polygon.api;

import java.net.http.HttpResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.http.Http;
import internal.ironhorse.stockinfo.config.APIConfig;
import internal.ironhorse.stockinfo.polygon.api.response.APIResponse;
import internal.ironhorse.stockinfo.polygon.api.response.CompanyV1Response;
import internal.ironhorse.stockinfo.polygon.api.response.ExchangesV3Response;
import internal.ironhorse.stockinfo.polygon.api.response.TickersV3Response;
import internal.ironhorse.stockinfo.polygon.util.EndpointBuilder;
import internal.ironhorse.stockinfo.polygon.util.JsonUtil;

/**
 * Direct access to the polygon API.
 * Transform DTO response classes to
 * Service wide library classes.
 */
@Service
public class APIService {
    private final Logger log = LoggerFactory.getLogger(APIService.class);
    private final APIConfig config;
    private final Http http;

    public APIService(APIConfig config, Http httpSvc) {
        this.config = config;
        this.http = httpSvc;
        String baseUrl = config.constructAPIEndpoint();
        this.http.setBaseURL(baseUrl);
        MDC.put("url.base", baseUrl);
    }

    public TickersV3Response performSearch(String ticker) throws RestException {
        MDC.put("api.search", ticker);
        return get(getEndpointBuilder(Endpoints.TICKERS).addParam("search", ticker), TickersV3Response.class);
    }

    public TickersV3Response getTicker(String ticker) throws RestException {
        MDC.put("api.ticker", ticker);
        return get(getEndpointBuilder(Endpoints.TICKERS).addParam("ticker", ticker), TickersV3Response.class);
    }

    public CompanyV1Response getCompany(String symbol) throws RestException {
        MDC.put("api.symbol", symbol);
        return get(getEndpointBuilder(Endpoints.COMPANY)
                .addSection(symbol.toUpperCase())
                .addSection("company"),
                CompanyV1Response.class
        );
    }

    public ExchangesV3Response getExchanges() throws RestException {
        return get(getEndpointBuilder(Endpoints.EXCHANGE),
            ExchangesV3Response.class
        );
    }

    private <T extends APIResponse> T get(EndpointBuilder endpoint, Class expectedClass) throws RestException{
        try{
            log.info(endpoint.build());
            HttpResponse<String> getResponse = http.GET(endpoint.build());
            HttpStatus status = HttpStatus.valueOf(getResponse.statusCode());
            MDC.put("api.statuscode", status.name());
            if(status.is4xxClientError() || status.is5xxServerError()){
                log.warn("API Exception");
                // was not successful - client or server error only
                throw new RestException(status.name(), new Exception(status.getReasonPhrase()));
            }
            log.info("SUMMARY");
            return JsonUtil.fromJson(getResponse.body(), expectedClass);
        }catch(Exception e) {
            log.error("ERROR", e);
            throw new RestException("Error", e);
        }
    }

    private EndpointBuilder getEndpointBuilder(Endpoints endpoint) {
        return EndpointBuilder.newBuilder()
                .addKey(config.getKey())
                .addSection(endpoint.getEndpoint());
    }

}
