package internal.ironhorse.stockinfo.polygon.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple builder pattern for query endpoints
 * Sample polygon endpoint:
 * reference/tickers?sort=ticker&search=Microsoft&perpage=50&page=1&apiKey=fCVW2qx9b1fs9SbMSgZhWCZ1RwzXSuyB
 */
public class EndpointBuilder {
    public final static String SEP = "/";
    private final static String QUERY = "?";
    private final static String AND = "&";
    private final static String EQUAL = "%s=%s";
    private final static String KEY = "apiKey=%s";

    private String apiKey;
    private final List<String> sections = new ArrayList<>();
    private final List<String> params = new ArrayList<>();

    private EndpointBuilder() {
    }

    public static EndpointBuilder newBuilder() {
        return new EndpointBuilder();
    }

    public EndpointBuilder addSection(String section) {
        sections.add(SEP + section);
        return this;
    }

    public EndpointBuilder addParam(String key, String value) {
        params.add(String.format(EQUAL, key, value));
        return this;
    }

    public EndpointBuilder addKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public String build() throws Exception {
        if (StringUtils.isBlank(this.apiKey)) {
            throw new Exception("key is required");
        }
        if (sections.isEmpty() && params.isEmpty()) {
            throw new Exception("require a section OR param");
        }

        String endpointSections = String.join("", sections);
        String queryParams = String.join(AND, params);

        // Polygon API always wants the APIKey with ampersand.
        queryParams = queryParams + AND + String.format(KEY, this.apiKey);

        return endpointSections + QUERY + queryParams;
    }

}
