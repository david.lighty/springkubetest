package internal.ironhorse.stockinfo.polygon.api;

/**
 * List of available endpoints
 */
public enum Endpoints {
    TICKERS("tickers", Endpoints.V3, Endpoints.REF),
    NEWS("news", Endpoints.V2, Endpoints.REF),
    COMPANY("symbols", Endpoints.V1, Endpoints.META),
    EXCHANGE("exchanges", Endpoints.V3, Endpoints.REF),
    ;

    // Leave public for custom endpoint building
    public final static String V3 = "v3";
    public final static String V2 = "v2";
    public final static String V1 = "v1";
    public final static String META = "meta";
    public final static String REF = "reference";

    private final String endpoint;

    Endpoints(String value, String version, String type){
        this.endpoint = String.format("%s/%s/%s", version, type, value);
    }

    public String getEndpoint(){
        return endpoint;
    }
}
