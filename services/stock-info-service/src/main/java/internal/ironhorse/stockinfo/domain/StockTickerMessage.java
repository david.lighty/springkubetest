package internal.ironhorse.stockinfo.domain;

public class StockTickerMessage extends EventMessage {

  private String tickerSymbol;

  public StockTickerMessage(String tickerSymbol) {
    this.tickerSymbol = tickerSymbol;
  }
}
