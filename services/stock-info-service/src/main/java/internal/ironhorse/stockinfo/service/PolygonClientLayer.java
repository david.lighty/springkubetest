package internal.ironhorse.stockinfo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import internal.ironhorse.exceptions.RestException;
import internal.ironhorse.http.Http;
import internal.ironhorse.library.api.Exchange;
import internal.ironhorse.library.api.Stock;
import internal.ironhorse.stockinfo.config.APIConfig;
import internal.ironhorse.stockinfo.polygon.api.APIService;
import internal.ironhorse.stockinfo.polygon.api.mappers.APIMapper;
import internal.ironhorse.stockinfo.polygon.api.response.CompanyV1Response;
import internal.ironhorse.stockinfo.polygon.api.response.ExchangesV3Response;
import internal.ironhorse.stockinfo.polygon.api.response.TickersV3Response;

public class PolygonClientLayer implements APIClientService{
    private final Logger log = LoggerFactory.getLogger(PolygonClientLayer.class);

    private final APIService apiService;
    private final APIMapper mapper;

    public PolygonClientLayer(APIConfig config, Http http, APIMapper mapper){
        this.mapper = mapper;
        apiService = new APIService(config, http);
    }

    @Override
    public List<Stock> searchStockInfo(String tickerName) throws RestException {
        log.info("Search "+ tickerName);
        TickersV3Response apiResponse = apiService.performSearch(tickerName.toUpperCase());
        return apiResponse.getResults().stream()
                .map(mapper::tickerToStock)
                .collect(Collectors.toList());
    }

    @Override
    public Stock getStockInfo(String stockTicker) throws RestException {
        log.info("Get Info "+ stockTicker);
        CompanyV1Response apiResponse = apiService.getCompany(stockTicker);
        return mapper.companyToStock(apiResponse);
    }

    @Override
    public void getStockNews(String stockTicker) {
        log.info("Get news: " + stockTicker);

    }

    @Override
    public Exchange getExchange(String name) {
//        log.info("Get Exchange " + name);
//        apiService.getExchange(name);
        return null;
    }

    @Override
    public List<Exchange> getExchanges() throws RestException {
        log.info("Get Exchanges");
        ExchangesV3Response resp = apiService.getExchanges();
        return resp.getResults().stream()
            .map(mapper::exchangeToExchange)
            .collect(Collectors.toList());
    }
}
