# Stock Info Service

API: Polygon
https://polygon.io/dashboard

This service handles our interface to the Polygon API 
to get stock vitals.  This service will publish kafka 
messages for each of the exposed API client methods.

Please ensure Kafka is up and running first, before starting 
this service.  If you are a local dev then your kafka
volume instance is *probably* ephemeral, which means
*topics* are not saved after re-start.

This service registers with a Eureka discovery server, 
in an attempt to be available through an API Gateway.

** Currently this works if everything is inside Docker. (todo list!)

Docker Compose
--------------
Start stock-info-service, from project root (not module root)

`docker-compose -f services-compose.yml up -d`

to scale: `--scale stockinfoservice=3`