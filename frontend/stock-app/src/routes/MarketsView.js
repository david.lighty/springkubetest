import MarketList from "../components/MarketList";
import {Breadcrumb} from "antd";

function MarketsView() {
  return (
      <>
        <Breadcrumb style={{margin: '16px 0'}}>
          <Breadcrumb.Item>Markets</Breadcrumb.Item>
        </Breadcrumb>
        <div className="site-layout-background"
             style={{padding: 24, minHeight: 360}}>
          <MarketList/>
        </div>
      </>
)
}

export default MarketsView;