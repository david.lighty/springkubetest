import {Breadcrumb} from "antd";
import StockInfoList from "../components/StockInfoList";

function StockSearch({stockList, input}) {
  return (
      <>
        <Breadcrumb style={{margin: '16px 0'}}>
          <Breadcrumb.Item>Search</Breadcrumb.Item>
          <Breadcrumb.Item>{input}</Breadcrumb.Item>
        </Breadcrumb>
        <div className="site-layout-background"
             style={{padding: 24, minHeight: 360}}>
          <StockInfoList stockList={stockList}/>
        </div>
      </>
  )
}

export default StockSearch;
