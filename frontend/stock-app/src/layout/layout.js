import {Layout, Space} from "antd";
import Footer from "./footer";
import SearchBar from "../components/SearchBar";
import {useState} from "react";
import {isEmpty} from "lodash";
import log from '../middleware/logger';
import {Route, Routes, useNavigate} from "react-router-dom";
import StockSearch from "../routes/StockSearch";
import MarketsView from "../routes/MarketsView";

const {Header, Content} = Layout;

function MainView() {
  const [input, setInput] = useState();
  const [stockList, setStockList] = useState();
  const navigate = useNavigate();

  const handleSearch = async (input) => {
    log.info(`Search: ${input}`);
    setInput(input);

    if(isEmpty(input) || input.length < 3){
      setStockList([]);
      return;
    }

    navigate("/search");

    // get search data from API
    fetch(`http://localhost:8080/info/search/${input}`)
    .then(res => {
     log.debug(`Response: ${JSON.stringify(res)}`)
      if(!res.ok) return [];
      return res.json();
    })
    .then((result) => {
      log.info(`result: ${JSON.stringify(result)}`);
      setStockList(result);
    })
    .catch(r => {
      log.error(`Failed ${r}`);
      setStockList([{name:"Failed to get response."}]);
    });

  }

  return (
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{padding: 0}}>
          <Space align="center">
            <SearchBar input={input} handleInput={handleSearch}/>
          </Space>
        </Header>
        <Content style={{margin: '0 16px'}}>
          <Routes>
            <Route path="/search" element={<StockSearch stockList={stockList} input={input} />} />
            <Route path="/markets" element={<MarketsView />} />
          </Routes>
        </Content>
        <Footer/>
      </Layout>
  )
}

export default MainView;