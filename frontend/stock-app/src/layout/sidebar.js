import React, {useState} from 'react';
import {Layout, Menu} from "antd";
import {FundViewOutlined, StockOutlined} from "@ant-design/icons";
import {Link, useLocation} from "react-router-dom";

const {Sider} = Layout;

function Sidebar() {
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation();
  const {pathname} = location;

  return (
      <Sider collapsible collapsed={collapsed}
             onCollapse={() => setCollapsed(!collapsed)}>
        <div className="logo"/>
        <Menu theme="dark" defaultSelectedKeys={['/search']} selectedKeys={[pathname]} mode="inline">
          <Menu.Item key="/search" icon={<FundViewOutlined/>}>
            <Link to="/search">Stock Search</Link>
          </Menu.Item>
          <Menu.Item key="/markets" icon={<StockOutlined/>}>
            <Link to="/markets">Markets</Link>
          </Menu.Item>
        </Menu>
      </Sider>
  )
}

export default Sidebar;