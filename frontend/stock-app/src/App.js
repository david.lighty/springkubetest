import {Layout} from 'antd';
import './App.css';
import MainView from "./layout/layout";
import Sidebar from "./layout/sidebar";


function App() {

  return (
      <Layout style={{minHeight: '100vh'}}>
        <Sidebar />
        <MainView />
      </Layout>
  );
}

export default App;