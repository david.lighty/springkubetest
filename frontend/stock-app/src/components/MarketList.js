import {Col, Row} from 'antd';
import './card.css';
import Exchange from "./Exchange";
import log from "../middleware/logger";
import {useEffect, useState} from "react";

function MarketList() {
  const [marketList, setMarketList] = useState([]);

  useEffect(() => {
    // get search data from API
    fetch(`http://localhost:8080/exchanges`)
    .then(res => {
      log.debug(`Response: ${JSON.stringify(res)}`)
      if (!res.ok) {
        return [];
      }
      return res.json();
    })
    .then((result) => {
      log.info(`result: ${JSON.stringify(result)}`);
      setMarketList(result);
    })
    .catch(r => {
      log.error(`Failed ${r}`);
      setMarketList([{name: "Failed to get response."}]);
    });
  }, []);

  if (!marketList || marketList.length === 0) {
    return (
        <div>Loading...</div>
    );
  }

  return (
      <>
        <Row gutter={[16,8]} wrap>
          {marketList.map((data, index) => {
            return (<Col className="gutter-box" xs={32} sm={32} md={24} lg={16} xl={6}>
              <Exchange exchange={data} idx={index} key={index}
                        className="card"/>
            </Col>);
          })}
        </Row>
        <div>
          Data: {marketList.length}
        </div>
      </>
  )
}

export default MarketList;