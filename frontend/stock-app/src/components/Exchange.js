import {Card} from "antd";

const {Meta} = Card;

function ExchangeCard({exchange, idx}) {
  return (
          <Card style={{width: "100%"}} key={idx}>
            <Meta title={exchange.name} description={exchange.locale}/>
            <div>
              <a href={exchange.url}>{exchange.url}</a>
              <div>
                <span>Type: {exchange.type}</span>
              </div>
            </div>
          </Card>
  );

}

export default ExchangeCard;