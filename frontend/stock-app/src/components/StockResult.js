import {Card, Collapse} from "antd";

const {Meta} = Card;
const {Panel} = Collapse;

function StockResult({stock, idx}) {
  return (
      <Collapse ghost>
        <Panel header={stock.name} key={idx}>
          <Card style={{width: "100%"}}>
            <Meta title={stock.name} description={stock.symbol}/>
          </Card>
        </Panel>
      </Collapse>
  );

}

export default StockResult;