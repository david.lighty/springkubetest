import {Input} from 'antd';

const {Search} = Input;

function SearchBar({input, handleInput}) {
  const onChange = e => handleInput(e.target.value);
  return (
      <Search placeholder="input search text"
              value={input}
              onChange={onChange}
              style={{width: 200}}
      />
  )
}

export default SearchBar;